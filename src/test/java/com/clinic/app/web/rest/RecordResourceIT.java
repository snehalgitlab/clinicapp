package com.clinic.app.web.rest;

import com.clinic.app.ClinicApp;
import com.clinic.app.domain.Record;
import com.clinic.app.repository.RecordRepository;
import com.clinic.app.service.RecordService;
import com.clinic.app.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.clinic.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RecordResource} REST controller.
 */
@SpringBootTest(classes = ClinicApp.class)
public class RecordResourceIT {

    private static final Instant DEFAULT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_PAST_HISTORY = "AAAAAAAAAA";
    private static final String UPDATED_PAST_HISTORY = "BBBBBBBBBB";

    private static final String DEFAULT_PRESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_PRESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_CLINICAL_DETAILS = "AAAAAAAAAA";
    private static final String UPDATED_CLINICAL_DETAILS = "BBBBBBBBBB";

    private static final String DEFAULT_INVESTIGATION = "AAAAAAAAAA";
    private static final String UPDATED_INVESTIGATION = "BBBBBBBBBB";

    private static final String DEFAULT_CURRENT_HISTORY = "AAAAAAAAAA";
    private static final String UPDATED_CURRENT_HISTORY = "BBBBBBBBBB";

    private static final String DEFAULT_REF_DOCTOR = "AAAAAAAAAA";
    private static final String UPDATED_REF_DOCTOR = "BBBBBBBBBB";

    private static final String DEFAULT_CHARGES = "AAAAAAAAAA";
    private static final String UPDATED_CHARGES = "BBBBBBBBBB";

    private static final String DEFAULT_DISCOUNT = "AAAAAAAAAA";
    private static final String UPDATED_DISCOUNT = "BBBBBBBBBB";

    private static final String DEFAULT_PAYMENT_HISTORY = "AAAAAAAAAA";
    private static final String UPDATED_PAYMENT_HISTORY = "BBBBBBBBBB";

    @Autowired
    private RecordRepository recordRepository;

    @Autowired
    private RecordService recordService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRecordMockMvc;

    private Record record;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RecordResource recordResource = new RecordResource(recordService);
        this.restRecordMockMvc = MockMvcBuilders.standaloneSetup(recordResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Record createEntity(EntityManager em) {
        Record record = new Record()
            .date(DEFAULT_DATE)
            .pastHistory(DEFAULT_PAST_HISTORY)
            .prescription(DEFAULT_PRESCRIPTION)
            .clinicalDetails(DEFAULT_CLINICAL_DETAILS)
            .investigation(DEFAULT_INVESTIGATION)
            .currentHistory(DEFAULT_CURRENT_HISTORY)
            .refDoctor(DEFAULT_REF_DOCTOR)
            .charges(DEFAULT_CHARGES)
            .discount(DEFAULT_DISCOUNT)
            .paymentHistory(DEFAULT_PAYMENT_HISTORY);
        return record;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Record createUpdatedEntity(EntityManager em) {
        Record record = new Record()
            .date(UPDATED_DATE)
            .pastHistory(UPDATED_PAST_HISTORY)
            .prescription(UPDATED_PRESCRIPTION)
            .clinicalDetails(UPDATED_CLINICAL_DETAILS)
            .investigation(UPDATED_INVESTIGATION)
            .currentHistory(UPDATED_CURRENT_HISTORY)
            .refDoctor(UPDATED_REF_DOCTOR)
            .charges(UPDATED_CHARGES)
            .discount(UPDATED_DISCOUNT)
            .paymentHistory(UPDATED_PAYMENT_HISTORY);
        return record;
    }

    @BeforeEach
    public void initTest() {
        record = createEntity(em);
    }

    @Test
    @Transactional
    public void createRecord() throws Exception {
        int databaseSizeBeforeCreate = recordRepository.findAll().size();

        // Create the Record
        restRecordMockMvc.perform(post("/api/records")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(record)))
            .andExpect(status().isCreated());

        // Validate the Record in the database
        List<Record> recordList = recordRepository.findAll();
        assertThat(recordList).hasSize(databaseSizeBeforeCreate + 1);
        Record testRecord = recordList.get(recordList.size() - 1);
        assertThat(testRecord.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testRecord.getPastHistory()).isEqualTo(DEFAULT_PAST_HISTORY);
        assertThat(testRecord.getPrescription()).isEqualTo(DEFAULT_PRESCRIPTION);
        assertThat(testRecord.getClinicalDetails()).isEqualTo(DEFAULT_CLINICAL_DETAILS);
        assertThat(testRecord.getInvestigation()).isEqualTo(DEFAULT_INVESTIGATION);
        assertThat(testRecord.getCurrentHistory()).isEqualTo(DEFAULT_CURRENT_HISTORY);
        assertThat(testRecord.getRefDoctor()).isEqualTo(DEFAULT_REF_DOCTOR);
        assertThat(testRecord.getCharges()).isEqualTo(DEFAULT_CHARGES);
        assertThat(testRecord.getDiscount()).isEqualTo(DEFAULT_DISCOUNT);
        assertThat(testRecord.getPaymentHistory()).isEqualTo(DEFAULT_PAYMENT_HISTORY);
    }

    @Test
    @Transactional
    public void createRecordWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = recordRepository.findAll().size();

        // Create the Record with an existing ID
        record.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRecordMockMvc.perform(post("/api/records")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(record)))
            .andExpect(status().isBadRequest());

        // Validate the Record in the database
        List<Record> recordList = recordRepository.findAll();
        assertThat(recordList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = recordRepository.findAll().size();
        // set the field null
        record.setDate(null);

        // Create the Record, which fails.

        restRecordMockMvc.perform(post("/api/records")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(record)))
            .andExpect(status().isBadRequest());

        List<Record> recordList = recordRepository.findAll();
        assertThat(recordList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPastHistoryIsRequired() throws Exception {
        int databaseSizeBeforeTest = recordRepository.findAll().size();
        // set the field null
        record.setPastHistory(null);

        // Create the Record, which fails.

        restRecordMockMvc.perform(post("/api/records")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(record)))
            .andExpect(status().isBadRequest());

        List<Record> recordList = recordRepository.findAll();
        assertThat(recordList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPrescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = recordRepository.findAll().size();
        // set the field null
        record.setPrescription(null);

        // Create the Record, which fails.

        restRecordMockMvc.perform(post("/api/records")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(record)))
            .andExpect(status().isBadRequest());

        List<Record> recordList = recordRepository.findAll();
        assertThat(recordList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkClinicalDetailsIsRequired() throws Exception {
        int databaseSizeBeforeTest = recordRepository.findAll().size();
        // set the field null
        record.setClinicalDetails(null);

        // Create the Record, which fails.

        restRecordMockMvc.perform(post("/api/records")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(record)))
            .andExpect(status().isBadRequest());

        List<Record> recordList = recordRepository.findAll();
        assertThat(recordList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkInvestigationIsRequired() throws Exception {
        int databaseSizeBeforeTest = recordRepository.findAll().size();
        // set the field null
        record.setInvestigation(null);

        // Create the Record, which fails.

        restRecordMockMvc.perform(post("/api/records")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(record)))
            .andExpect(status().isBadRequest());

        List<Record> recordList = recordRepository.findAll();
        assertThat(recordList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCurrentHistoryIsRequired() throws Exception {
        int databaseSizeBeforeTest = recordRepository.findAll().size();
        // set the field null
        record.setCurrentHistory(null);

        // Create the Record, which fails.

        restRecordMockMvc.perform(post("/api/records")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(record)))
            .andExpect(status().isBadRequest());

        List<Record> recordList = recordRepository.findAll();
        assertThat(recordList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRefDoctorIsRequired() throws Exception {
        int databaseSizeBeforeTest = recordRepository.findAll().size();
        // set the field null
        record.setRefDoctor(null);

        // Create the Record, which fails.

        restRecordMockMvc.perform(post("/api/records")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(record)))
            .andExpect(status().isBadRequest());

        List<Record> recordList = recordRepository.findAll();
        assertThat(recordList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkChargesIsRequired() throws Exception {
        int databaseSizeBeforeTest = recordRepository.findAll().size();
        // set the field null
        record.setCharges(null);

        // Create the Record, which fails.

        restRecordMockMvc.perform(post("/api/records")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(record)))
            .andExpect(status().isBadRequest());

        List<Record> recordList = recordRepository.findAll();
        assertThat(recordList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDiscountIsRequired() throws Exception {
        int databaseSizeBeforeTest = recordRepository.findAll().size();
        // set the field null
        record.setDiscount(null);

        // Create the Record, which fails.

        restRecordMockMvc.perform(post("/api/records")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(record)))
            .andExpect(status().isBadRequest());

        List<Record> recordList = recordRepository.findAll();
        assertThat(recordList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPaymentHistoryIsRequired() throws Exception {
        int databaseSizeBeforeTest = recordRepository.findAll().size();
        // set the field null
        record.setPaymentHistory(null);

        // Create the Record, which fails.

        restRecordMockMvc.perform(post("/api/records")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(record)))
            .andExpect(status().isBadRequest());

        List<Record> recordList = recordRepository.findAll();
        assertThat(recordList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRecords() throws Exception {
        // Initialize the database
        recordRepository.saveAndFlush(record);

        // Get all the recordList
        restRecordMockMvc.perform(get("/api/records?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(record.getId().intValue())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].pastHistory").value(hasItem(DEFAULT_PAST_HISTORY)))
            .andExpect(jsonPath("$.[*].prescription").value(hasItem(DEFAULT_PRESCRIPTION)))
            .andExpect(jsonPath("$.[*].clinicalDetails").value(hasItem(DEFAULT_CLINICAL_DETAILS)))
            .andExpect(jsonPath("$.[*].investigation").value(hasItem(DEFAULT_INVESTIGATION)))
            .andExpect(jsonPath("$.[*].currentHistory").value(hasItem(DEFAULT_CURRENT_HISTORY)))
            .andExpect(jsonPath("$.[*].refDoctor").value(hasItem(DEFAULT_REF_DOCTOR)))
            .andExpect(jsonPath("$.[*].charges").value(hasItem(DEFAULT_CHARGES)))
            .andExpect(jsonPath("$.[*].discount").value(hasItem(DEFAULT_DISCOUNT)))
            .andExpect(jsonPath("$.[*].paymentHistory").value(hasItem(DEFAULT_PAYMENT_HISTORY)));
    }
    
    @Test
    @Transactional
    public void getRecord() throws Exception {
        // Initialize the database
        recordRepository.saveAndFlush(record);

        // Get the record
        restRecordMockMvc.perform(get("/api/records/{id}", record.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(record.getId().intValue()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.pastHistory").value(DEFAULT_PAST_HISTORY))
            .andExpect(jsonPath("$.prescription").value(DEFAULT_PRESCRIPTION))
            .andExpect(jsonPath("$.clinicalDetails").value(DEFAULT_CLINICAL_DETAILS))
            .andExpect(jsonPath("$.investigation").value(DEFAULT_INVESTIGATION))
            .andExpect(jsonPath("$.currentHistory").value(DEFAULT_CURRENT_HISTORY))
            .andExpect(jsonPath("$.refDoctor").value(DEFAULT_REF_DOCTOR))
            .andExpect(jsonPath("$.charges").value(DEFAULT_CHARGES))
            .andExpect(jsonPath("$.discount").value(DEFAULT_DISCOUNT))
            .andExpect(jsonPath("$.paymentHistory").value(DEFAULT_PAYMENT_HISTORY));
    }

    @Test
    @Transactional
    public void getNonExistingRecord() throws Exception {
        // Get the record
        restRecordMockMvc.perform(get("/api/records/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRecord() throws Exception {
        // Initialize the database
        recordService.save(record);

        int databaseSizeBeforeUpdate = recordRepository.findAll().size();

        // Update the record
        Record updatedRecord = recordRepository.findById(record.getId()).get();
        // Disconnect from session so that the updates on updatedRecord are not directly saved in db
        em.detach(updatedRecord);
        updatedRecord
            .date(UPDATED_DATE)
            .pastHistory(UPDATED_PAST_HISTORY)
            .prescription(UPDATED_PRESCRIPTION)
            .clinicalDetails(UPDATED_CLINICAL_DETAILS)
            .investigation(UPDATED_INVESTIGATION)
            .currentHistory(UPDATED_CURRENT_HISTORY)
            .refDoctor(UPDATED_REF_DOCTOR)
            .charges(UPDATED_CHARGES)
            .discount(UPDATED_DISCOUNT)
            .paymentHistory(UPDATED_PAYMENT_HISTORY);

        restRecordMockMvc.perform(put("/api/records")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedRecord)))
            .andExpect(status().isOk());

        // Validate the Record in the database
        List<Record> recordList = recordRepository.findAll();
        assertThat(recordList).hasSize(databaseSizeBeforeUpdate);
        Record testRecord = recordList.get(recordList.size() - 1);
        assertThat(testRecord.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testRecord.getPastHistory()).isEqualTo(UPDATED_PAST_HISTORY);
        assertThat(testRecord.getPrescription()).isEqualTo(UPDATED_PRESCRIPTION);
        assertThat(testRecord.getClinicalDetails()).isEqualTo(UPDATED_CLINICAL_DETAILS);
        assertThat(testRecord.getInvestigation()).isEqualTo(UPDATED_INVESTIGATION);
        assertThat(testRecord.getCurrentHistory()).isEqualTo(UPDATED_CURRENT_HISTORY);
        assertThat(testRecord.getRefDoctor()).isEqualTo(UPDATED_REF_DOCTOR);
        assertThat(testRecord.getCharges()).isEqualTo(UPDATED_CHARGES);
        assertThat(testRecord.getDiscount()).isEqualTo(UPDATED_DISCOUNT);
        assertThat(testRecord.getPaymentHistory()).isEqualTo(UPDATED_PAYMENT_HISTORY);
    }

    @Test
    @Transactional
    public void updateNonExistingRecord() throws Exception {
        int databaseSizeBeforeUpdate = recordRepository.findAll().size();

        // Create the Record

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRecordMockMvc.perform(put("/api/records")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(record)))
            .andExpect(status().isBadRequest());

        // Validate the Record in the database
        List<Record> recordList = recordRepository.findAll();
        assertThat(recordList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRecord() throws Exception {
        // Initialize the database
        recordService.save(record);

        int databaseSizeBeforeDelete = recordRepository.findAll().size();

        // Delete the record
        restRecordMockMvc.perform(delete("/api/records/{id}", record.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Record> recordList = recordRepository.findAll();
        assertThat(recordList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
