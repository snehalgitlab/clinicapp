package com.clinic.app.repository;

import java.sql.Date;
import java.time.Instant;
import java.util.List;

import javax.persistence.EntityManager;

import com.clinic.app.domain.Record;

import org.springframework.data.jpa.repository.*;

import org.springframework.data.repository.query.Param;


/**
 * Spring Data  repository for the Record entity.
 */

 //public interface RecordRepository extends CrudRepository<Record,Long>{
public interface RecordRepository extends JpaRepository<Record, Long> {

    
    @Query("select r from Record r where r.patient.id = :userid")
    List<Record> findAllRecordsBypatientId(@Param("userid") Long userid);

    //@Query("select r from Record r where r.date between :startdate and :enddate  order by r.refDoctor")
    //List<Record> findAllPatientRefByDoctor(@Param("startdate") Instant startdate, @Param("enddate") Instant enddate);

    @Query("select r from Record r where r.refDoctor = :refDoctor")
    List<Record> findAllPatientRefByDoctor(@Param("refDoctor") String refDoctor);

    }

