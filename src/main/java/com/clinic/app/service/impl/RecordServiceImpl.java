package com.clinic.app.service.impl;

import com.clinic.app.service.RecordService;
import com.mysql.cj.Query;
import com.clinic.app.domain.Record;
import com.clinic.app.repository.RecordRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

/**
 * Service Implementation for managing {@link Record}.
 */
@Service
@Transactional
public class RecordServiceImpl implements RecordService {

    private final Logger log = LoggerFactory.getLogger(RecordServiceImpl.class);

    private final RecordRepository recordRepository;

   
   


    public RecordServiceImpl(RecordRepository recordRepository) {
       // @Autowired
        this.recordRepository = recordRepository;
    }

    /**
     * Save a record.
     *
     * @param record the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Record save(Record record) {
        log.debug("Request to save Record : {}", record);
        return recordRepository.save(record);
    }

    /**
     * Get all the records.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<Record> findAll() {
        log.debug("Request to get all Records");
        return recordRepository.findAll();
    }

    
    /* Get all the records.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<Record> findAllRecordsBypatientId(Long id) {
        log.debug("Request to get all Records im service Impl class");
        System.out.println("inside find all By id in service impl:" + id);
     
        return recordRepository.findAllRecordsBypatientId(id);
       
    }

   
    /* Get all the records.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<Record> findAllPatientRefByDoctor(String Refdoc) {
        log.debug("Request to get all Records im service Impl class");
        
        return recordRepository.findAllPatientRefByDoctor(Refdoc);
       
    }


    /**
     * Get one record by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Record> findOne(Long id) {
        log.debug("Request to get Record : {}", id);
        return recordRepository.findById(id);
    }

    /**
     * Delete the record by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Record : {}", id);
        recordRepository.deleteById(id);
    }

    

    @Override
    public List<Record> findAllById(Long id) {
        // TODO Auto-generated method stub
        System.out.println("FIND ALL BY ID");
        return null;
    }

}
