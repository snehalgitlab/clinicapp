package com.clinic.app.service;

import com.clinic.app.domain.Record;

import org.springframework.http.ResponseEntity;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Record}.
 */
public interface RecordService {


    
    /**
     * Save a record.
     *
     * @param record the entity to save.
     * @return the persisted entity.
     */
    Record save(Record record);

    /**
     * Get all the records.
     *
     * @return the list of entities.
     */
    List<Record> findAll();

    /**
     * Get all the records.
     *
     * @return the list of entities.
     */
    List<Record> findAllById(Long id);

    /**
     * Get all the records.
     *
     * @return the list of entities.
     */
    List<Record> findAllRecordsBypatientId(Long id);

    /**
     * Get all the records.
     *
     * @return the list of entities.
     */
    //List<Record> findAllPatientRefByDoctor(Instant startdate,Instant enddate);
    List<Record> findAllPatientRefByDoctor(String RefDoc);

    /**
     * Get the "id" record.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Record> findOne(Long id);

    /**
     * Delete the "id" record.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

	
}
