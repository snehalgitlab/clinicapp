/**
 * View Models used by Spring MVC REST controllers.
 */
package com.clinic.app.web.rest.vm;
