package com.clinic.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;

/**
 * A Record.
 */
@Entity
@Table(name = "record")
public class Record implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "date", nullable = false)
    private Instant date;

    @NotNull
    @Column(name = "past_history", nullable = false)
    private String pastHistory;

    @NotNull
    @Column(name = "prescription", nullable = false)
    private String prescription;

    @NotNull
    @Column(name = "clinical_details", nullable = false)
    private String clinicalDetails;

    @NotNull
    @Column(name = "investigation", nullable = false)
    private String investigation;

    @NotNull
    @Column(name = "current_history", nullable = false)
    private String currentHistory;

    @NotNull
    @Column(name = "ref_doctor", nullable = false)
    private String refDoctor;

    @NotNull
    @Column(name = "charges", nullable = false)
    private String charges;

    @NotNull
    @Column(name = "discount", nullable = false)
    private String discount;

    @NotNull
    @Column(name = "payment_history", nullable = false)
    private String paymentHistory;

    @ManyToOne
    @JsonIgnoreProperties("records")
    private Patient patient;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDate() {
        return date;
    }

    public Record date(Instant date) {
        this.date = date;
        return this;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public String getPastHistory() {
        return pastHistory;
    }

    public Record pastHistory(String pastHistory) {
        this.pastHistory = pastHistory;
        return this;
    }

    public void setPastHistory(String pastHistory) {
        this.pastHistory = pastHistory;
    }

    public String getPrescription() {
        return prescription;
    }

    public Record prescription(String prescription) {
        this.prescription = prescription;
        return this;
    }

    public void setPrescription(String prescription) {
        this.prescription = prescription;
    }

    public String getClinicalDetails() {
        return clinicalDetails;
    }

    public Record clinicalDetails(String clinicalDetails) {
        this.clinicalDetails = clinicalDetails;
        return this;
    }

    public void setClinicalDetails(String clinicalDetails) {
        this.clinicalDetails = clinicalDetails;
    }

    public String getInvestigation() {
        return investigation;
    }

    public Record investigation(String investigation) {
        this.investigation = investigation;
        return this;
    }

    public void setInvestigation(String investigation) {
        this.investigation = investigation;
    }

    public String getCurrentHistory() {
        return currentHistory;
    }

    public Record currentHistory(String currentHistory) {
        this.currentHistory = currentHistory;
        return this;
    }

    public void setCurrentHistory(String currentHistory) {
        this.currentHistory = currentHistory;
    }

    public String getRefDoctor() {
        return refDoctor;
    }

    public Record refDoctor(String refDoctor) {
        this.refDoctor = refDoctor;
        return this;
    }

    public void setRefDoctor(String refDoctor) {
        this.refDoctor = refDoctor;
    }

    public String getCharges() {
        return charges;
    }

    public Record charges(String charges) {
        this.charges = charges;
        return this;
    }

    public void setCharges(String charges) {
        this.charges = charges;
    }

    public String getDiscount() {
        return discount;
    }

    public Record discount(String discount) {
        this.discount = discount;
        return this;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getPaymentHistory() {
        return paymentHistory;
    }

    public Record paymentHistory(String paymentHistory) {
        this.paymentHistory = paymentHistory;
        return this;
    }

    public void setPaymentHistory(String paymentHistory) {
        this.paymentHistory = paymentHistory;
    }

    public Patient getPatient() {
        return patient;
    }

    public Record patient(Patient patient) {
        this.patient = patient;
        return this;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Record)) {
            return false;
        }
        return id != null && id.equals(((Record) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Record{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", pastHistory='" + getPastHistory() + "'" +
            ", prescription='" + getPrescription() + "'" +
            ", clinicalDetails='" + getClinicalDetails() + "'" +
            ", investigation='" + getInvestigation() + "'" +
            ", currentHistory='" + getCurrentHistory() + "'" +
            ", refDoctor='" + getRefDoctor() + "'" +
            ", charges='" + getCharges() + "'" +
            ", discount='" + getDiscount() + "'" +
            ", paymentHistory='" + getPaymentHistory() + "'" +
            "}";
    }
}
