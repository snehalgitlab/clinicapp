package com.clinic.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;

/**
 * A Record.
 */

public class Rquery implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Column(name = "startdate", nullable = false)
    private Instant startdate;


    @NotNull
    @Column(name = "enddate", nullable = false)
    private Instant enddate;

    
    @NotNull
    @Column(name = "ref_doctor", nullable = false)
    private String refDoctor;

  

   
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
   
    public Instant getStartDate() {
        return startdate;
    }

    public Rquery startdate(Instant startdate) {
        this.startdate =startdate;
        return this;
    }

    public void setStartDate(Instant startdate) {
        this.startdate=startdate;
    }


    public Instant getEndDate() {
        return enddate;
    }

    public Rquery enddate(Instant enddate) {
        this.enddate=enddate;
        return this;
    }

    public void setEndDate(Instant enddate) {
        this.enddate=enddate;
    }

    public String getRefDoctor() {
        return refDoctor;
    }

    public Rquery refDoctor(String refDoctor) {
        this.refDoctor = refDoctor;
        return this;
    }

    public void setRefDoctor(String refDoctor) {
        this.refDoctor = refDoctor;
    }

   


    @Override
    public String toString() {
        return "Rquery{" +
            
            ", startdate='" + getStartDate() + "'" +
            ", enddate='" + getEndDate() + "'" +
            ", refDoctor='" + getRefDoctor() ;
          
    }
}
