import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ClinicAppSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';

@NgModule({
  imports: [ClinicAppSharedModule, RouterModule.forChild([HOME_ROUTE])],
  declarations: [HomeComponent]
})
export class ClinicAppHomeModule {}
