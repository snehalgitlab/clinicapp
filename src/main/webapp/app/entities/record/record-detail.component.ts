import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRecord } from 'app/shared/model/record.model';
import { RecordDeleteDialogComponent } from './record-delete-dialog.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

@Component({
  selector: 'jhi-record-detail',
  templateUrl: './record-detail.component.html'
})
export class RecordDetailComponent implements OnInit {
  record: IRecord | null = null;
  records?: IRecord[];
  eventSubscriber?: Subscription;

  constructor(protected activatedRoute: ActivatedRoute, protected modalService: NgbModal, protected eventManager: JhiEventManager) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ record }) => (this.records = record) || []);
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  delete(record: IRecord): void {
    const modalRef = this.modalService.open(RecordDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.record = record;
  }
}
