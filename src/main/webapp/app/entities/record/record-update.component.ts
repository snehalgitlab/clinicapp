import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IRecord, Record } from 'app/shared/model/record.model';
import { RecordService } from './record.service';
import { IPatient } from 'app/shared/model/patient.model';
import { PatientService } from 'app/entities/patient/patient.service';

@Component({
  selector: 'jhi-record-update',
  templateUrl: './record-update.component.html'
})
export class RecordUpdateComponent implements OnInit {
  isSaving = false;

  records: IRecord[] = [];

  editForm = this.fb.group({
    id: [],
    date: [null, [Validators.required]],
    pastHistory: [null, [Validators.required]],
    prescription: [null, [Validators.required]],
    clinicalDetails: [null, [Validators.required]],
    investigation: [null, [Validators.required]],
    currentHistory: [null, [Validators.required]],
    refDoctor: [null, [Validators.required]],
    charges: [null, [Validators.required]],
    discount: [null, [Validators.required]],
    paymentHistory: [null, [Validators.required]],
    patient: []
  });

  constructor(
    protected recordService: RecordService,
    //protected patientService: PatientService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ record }) => {
      if (!record.id) {
        const today = moment().startOf('day');
        record.date = today;
      }

      this.updateForm(record);

      this.recordService.query().subscribe((res: HttpResponse<IRecord[]>) => (this.records = res.body || []));
    });
  }

  updateForm(record: IRecord): void {
    this.editForm.patchValue({
      id: record.id,
      date: record.date ? record.date.format(DATE_TIME_FORMAT) : null,
      pastHistory: record.pastHistory,
      prescription: record.prescription,
      clinicalDetails: record.clinicalDetails,
      investigation: record.investigation,
      currentHistory: record.currentHistory,
      refDoctor: record.refDoctor,
      charges: record.charges,
      discount: record.discount,
      paymentHistory: record.paymentHistory,
      patient: record.patient
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const record = this.createFromForm();
    if (record.id !== undefined) {
      this.subscribeToSaveResponse(this.recordService.update(record));
    } else {
      this.subscribeToSaveResponse(this.recordService.create(record));
    }
  }

  private createFromForm(): IRecord {
    return {
      ...new Record(),
      id: this.editForm.get(['id'])!.value,
      date: this.editForm.get(['date'])!.value ? moment(this.editForm.get(['date'])!.value, DATE_TIME_FORMAT) : undefined,
      pastHistory: this.editForm.get(['pastHistory'])!.value,
      prescription: this.editForm.get(['prescription'])!.value,
      clinicalDetails: this.editForm.get(['clinicalDetails'])!.value,
      investigation: this.editForm.get(['investigation'])!.value,
      currentHistory: this.editForm.get(['currentHistory'])!.value,
      refDoctor: this.editForm.get(['refDoctor'])!.value,
      charges: this.editForm.get(['charges'])!.value,
      discount: this.editForm.get(['discount'])!.value,
      paymentHistory: this.editForm.get(['paymentHistory'])!.value,
      patient: this.editForm.get(['patient'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRecord>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IPatient): any {
    return item.id;
  }
}
