import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { RecordService } from './record.service';

import { IRquery, Rquery } from 'app/shared/model/rquery.model';
import { IRecord, Record } from 'app/shared/model/record.model';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import * as moment from 'moment';
import { Subscription } from 'rxjs/internal/Subscription';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'jhi-record',
  templateUrl: './record.component.html'
})
export class RecordComponent implements OnInit {
  isSaving = false;
  record: IRecord | null = null;
  records?: IRecord[];
  eventSubscriber?: Subscription;

  editForm = this.fb.group({
    refDoctor: [null, [Validators.required]]
  });

  constructor(
    protected recordService: RecordService,

    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.recordService.query().subscribe((res: HttpResponse<IRecord[]>) => (this.records = res.body || []));
  }

  ngOnInit(): void {
    //this.loadAll();
    //this.registerChangeInRecords();
  }

  trackId(index: number, item: IRecord): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInRecords(): void {
    this.eventSubscriber = this.eventManager.subscribe('recordListModification', () => this.loadAll());
  }

  getAllRecordsByRefDoctor(): void {
    const rquery = this.createFromForm();
    this.recordService.findRecordByRef(rquery); //subscribe((res: HttpResponse<IRecord[]>) => this.records = res.body || []);
    this.eventSubscriber = this.eventManager.subscribe('RecordListByReferacne', () => (res: HttpResponse<IRecord[]>) =>
      (this.records = res.body || [])
    );
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  private createFromForm(): IRquery {
    return {
      ...new Rquery(),
      refDoctor: this.editForm.get(['refDoctor'])!.value

      //startdate: this.editForm.get(['date'])!.value ? moment(this.editForm.get(['date'])!.value, DATE_TIME_FORMAT) : undefined,
      //enddate: this.editForm.get(['date'])!.value ? moment(this.editForm.get(['date'])!.value, DATE_TIME_FORMAT) : undefined,

      //enddate: this.editForm.get(['enddate'])!.value,
    };
  }
}
