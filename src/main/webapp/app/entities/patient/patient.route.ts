import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IPatient, Patient } from 'app/shared/model/patient.model';
import { PatientService } from './patient.service';
import { PatientComponent } from './patient.component';
import { PatientDetailComponent } from './patient-detail.component';
import { PatientUpdateComponent } from './patient-update.component';
import { RecordDetailComponent } from '../record/record-detail.component';
import { RecordResolve } from '../record/record.route';
import { RecordComponent } from '../record/record.component';
import { RecordUpdateComponent } from '../record/record-update.component';
//import { record.route } from '../record/record.route'

@Injectable({ providedIn: 'root' })
export class PatientResolve implements Resolve<IPatient> {
  constructor(private service: PatientService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPatient> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((patient: HttpResponse<Patient>) => {
          if (patient.body) {
            return of(patient.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Patient());
  }
}

export const patientRoute: Routes = [
  {
    path: '',
    component: PatientComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Patients'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: RecordDetailComponent,
    resolve: {
      record: RecordResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Records'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: PatientUpdateComponent,
    resolve: {
      patient: PatientResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Patients'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: PatientUpdateComponent,
    resolve: {
      patient: PatientResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Patients'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'newrecord',
    component: RecordUpdateComponent,
    resolve: {
      record: RecordResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Record'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/recordedit',
    component: RecordUpdateComponent,
    resolve: {
      record: RecordResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Record'
    },
    canActivate: [UserRouteAccessService]
  }
];
