import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ClinicAppSharedModule } from 'app/shared/shared.module';
import { PatientComponent } from './patient.component';
import { PatientDetailComponent } from './patient-detail.component';
import { PatientUpdateComponent } from './patient-update.component';
import { PatientDeleteDialogComponent } from './patient-delete-dialog.component';
import { patientRoute } from './patient.route';
import { RecordDetailComponent } from '../record/record-detail.component';
import { RecordComponent } from '../record/record.component';
import { RecordUpdateComponent } from '../record/record-update.component';
import { RecordDeleteDialogComponent } from '../record/record-delete-dialog.component';

@NgModule({
  imports: [ClinicAppSharedModule, RouterModule.forChild(patientRoute)],
  declarations: [
    PatientComponent,
    RecordDetailComponent,
    PatientUpdateComponent,
    RecordUpdateComponent,
    PatientDeleteDialogComponent,
    RecordDeleteDialogComponent
  ],
  entryComponents: [PatientDeleteDialogComponent, RecordDeleteDialogComponent]
})
export class ClinicAppPatientModule {}
