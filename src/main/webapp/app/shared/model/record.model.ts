import { Moment } from 'moment';
import { IPatient } from 'app/shared/model/patient.model';

export interface IRecord {
  id?: number;
  date?: Moment;
  pastHistory?: string;
  prescription?: string;
  clinicalDetails?: string;
  investigation?: string;
  currentHistory?: string;
  refDoctor?: string;
  charges?: string;
  discount?: string;
  paymentHistory?: string;
  patient?: IPatient;
}

export class Record implements IRecord {
  constructor(
    public id?: number,
    public date?: Moment,
    public pastHistory?: string,
    public prescription?: string,
    public clinicalDetails?: string,
    public investigation?: string,
    public currentHistory?: string,
    public refDoctor?: string,
    public charges?: string,
    public discount?: string,
    public paymentHistory?: string,
    public patient?: IPatient
  ) {}
}
