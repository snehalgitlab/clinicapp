import { IRecord } from 'app/shared/model/record.model';

export interface IPatient {
  id?: number;
  patientName?: string;
  mobileNumber?: string;
  password?: string;
  address?: string;
  records?: IRecord[];
}

export class Patient implements IPatient {
  constructor(
    public id?: number,
    public patientName?: string,
    public mobileNumber?: string,
    public password?: string,
    public address?: string,
    public records?: IRecord[]
  ) {}
}
