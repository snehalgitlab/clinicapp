import { Moment } from 'moment';
import { IPatient } from 'app/shared/model/patient.model';
import { IRecord } from './record.model';

export interface IRquery {
  //id?: number;
  startdate?: Moment;
  enddate?: Moment;

  refDoctor?: string;
}

export class Rquery implements IRquery {
  constructor(
    public startdate?: Moment,
    public enddate?: Moment,

    public refDoctor?: string
  ) {}
}
