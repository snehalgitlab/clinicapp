export interface IAppointment {
  id?: number;
  time?: string;
  venue?: string;
  confirmation?: string;
}

export class Appointment implements IAppointment {
  constructor(public id?: number, public time?: string, public venue?: string, public confirmation?: string) {}
}
