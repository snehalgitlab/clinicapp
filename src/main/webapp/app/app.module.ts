import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { ClinicAppSharedModule } from 'app/shared/shared.module';
import { ClinicAppCoreModule } from 'app/core/core.module';
import { ClinicAppAppRoutingModule } from './app-routing.module';
import { ClinicAppHomeModule } from './home/home.module';
import { ClinicAppEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    ClinicAppSharedModule,
    ClinicAppCoreModule,
    ClinicAppHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    ClinicAppEntityModule,
    ClinicAppAppRoutingModule
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent]
})
export class ClinicAppAppModule {}
